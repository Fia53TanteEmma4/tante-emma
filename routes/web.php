<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@Index');
Route::get('/test', 'HomeController@Test');

Route::get('/artikel', 'ArticleController@Index');
Route::post('/artikel', 'ArticleController@Store');
Route::delete('/artikel/{article}','ArticleController@Destroy');
Route::patch('/artikel/{article}','ArticleController@Update');

Route::get('/bestellungen', 'OrderController@Index');
Route::post('/bestellungen', 'OrderController@Store');
Route::get('/bestellung/bearbeiten/{order}', 'OrderController@Edit');
Route::patch('/bestellung/{order}/abschluss', 'OrderController@closeOrder');
Route::post('/bestellung/{order}/artikel/{article}', 'OrderController@addArticle');
Route::delete('/bestellung/{order}/artikel/{article}', 'OrderController@removeArticle');
Route::delete('/bestellungen', 'OrderController@Destroy');


Route::get('/kunden','CustomerController@Index');
Route::post('/kunden','CustomerController@Store');
Route::delete('/kunden/{customer}', 'CustomerController@Destroy');
Route::patch('/kunden/{customer}', 'CustomerController@Update');


Route::get('/mitarbeiter','EmployerController@Index');
Route::post('/mitarbeiter','EmployerController@Store');
Route::delete('/mitarbeiter/{employer}', 'EmployerController@Destroy');
Route::patch('/mitarbeiter/{employer}', 'EmployerController@Update');
