$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2010 Q1',
            bestellungen: 2666,
            Artikel: 1232,
            umsatz: 2647
        }, {
            period: '2010 Q2',
            bestellungen: 2778,
            Artikel: 2294,
            umsatz: 2441
        }, {
            period: '2010 Q3',
            bestellungen: 4912,
            Artikel: 1969,
            umsatz: 2501
        }, {
            period: '2010 Q4',
            bestellungen: 3767,
            Artikel: 3597,
            umsatz: 5689
        }, {
            period: '2011 Q1',
            bestellungen: 6810,
            Artikel: 1914,
            umsatz: 2293
        }, {
            period: '2011 Q2',
            bestellungen: 5670,
            Artikel: 4293,
            umsatz: 1881
        }, {
            period: '2011 Q3',
            bestellungen: 4820,
            Artikel: 3795,
            umsatz: 1588
        }, {
            period: '2011 Q4',
            bestellungen: 15073,
            Artikel: 5967,
            umsatz: 5175
        }, {
            period: '2012 Q1',
            bestellungen: 10687,
            Artikel: 4460,
            umsatz: 2028
        }, {
            period: '2012 Q2',
            bestellungen: 8432,
            Artikel: 5713,
            umsatz: 1791
        }],
        xkey: 'period',
        ykeys: ['bestellungen', 'Artikel', 'umsatz'],
        labels: ['Bestellungen', 'Artikel', 'Umsatz'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });


    
});
