$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function(){

    $('#articles').on( 'click', 'tbody td:not(:last-child).editable', function (e) {
        $.each($(this).parent().find("td.editable"), function(){
            articleBuffer[$(this).data('name')] = $(this).text().trim();
             var steps = $(this).data("numberstep") ? ' step="' + $(this).data("numberstep") + '" ' : '';
            $(this).html('<input type="' + $(this).data("formtype") + '"' + steps + ' class="form-control" value="' + $(this).text().trim() + '" />');
            $(this).toggleClass('editable editmode');
        });
        $(this).parent().find('td:last-child').hasClass('editbuttons')?{}:
            $(this).parent().find('td:last-child')
                .prepend('<button class="btn btn-default fa fa-check save-changes"></button><button class="btn btn-default fa fa-times dismiss-changes"></button>')
                .addClass('editbuttons');
    } );
    $('#articles').on( 'click', 'button.delete-column', function () {
        var row = $(this).parent().parent();
         var id = row.data("orderid");
        $.ajax({
            url: '/bestellungen/'+id,
            type: 'delete',
        }).done(function (d) {
            if(d==1)
                $(row).fadeOut("slow", function() { $(this).remove(); } );

            //errorhandling
        });
    });


    $('#articles').DataTable();
});

var customerBuffer = {};
$(document).ready(function(){
    $('#customers').on( 'click', 'tbody td:not(:last-child).editable', function (e) {
        $.each($(this).parent().find("td.editable"), function(){
            customerBuffer[$(this).data('name')] = $(this).text().trim();
            var steps = $(this).data("numberstep") ? ' step="' + $(this).data("numberstep") + '" ' : '';
            $(this).html('<input type="' + $(this).data("formtype") + '"' + steps + ' class="form-control col-md-12" value="' + $(this).text().trim() + '" />');
            $(this).toggleClass('editable editmode');
        });
        $(this).parent().find('td:last-child').hasClass('editbuttons')?{}:
            $(this).parent().find('td:last-child')
                .prepend('<button class="btn btn-default fa fa-check save-changes"></button><button class="btn btn-default fa fa-times dismiss-changes"></button>')
                .addClass('editbuttons');
    } );
    $('#customers').on( 'click', 'button.delete-column', function () {
        var row = $(this).parent().parent();
        var id = row.data("customerid");
        $.ajax({
            url: '/kunden/'+id,
            type: 'delete',
        }).done(function (d) {
            if(d==1)
                $(row).fadeOut("slow", function() { $(this).remove(); } );

            //errorhandling
        });
    });
    $('#customers').on( 'click', 'button.save-changes', function () {
        var data = {};
        var row = $(this).parent().parent();
        data.id = row.data("customerid");

        $.each(row.find("td.editmode"), function(){
            data[$(this).data('name')] = $(this).find('input').val();
            $(this).text($(this).find('input').val());
            $(this).toggleClass('editable editmode');
        });
        $.ajax({
            url: '/kunden/'+data.id,
            type: 'patch',
            data: data
        }).done(function () {
            $(row).find('button.save-changes').parent().removeClass('editbuttons')
            $(row).find('button.save-changes').remove();
            $(row).find('button.dismiss-changes').remove();
        });
    });
    $('#customers').on( 'click', 'button.dismiss-changes', function () {
        var row = $(this).parent().parent();

        $.each(row.find("td.editmode"), function(){
            $(this).text(customerBuffer[$(this).data('name')]);
            $(this).toggleClass('editable editmode');
        });

        $(row).find('button.save-changes').parent().removeClass('editbuttons')
        $(row).find('button.save-changes').remove();
        $(row).find('button.dismiss-changes').remove();
    });


    $('#customers').DataTable();
});

var employerBuffer = {};
$(document).ready(function(){
    $('#employers').on( 'click', 'tbody td:not(:last-child).editable', function (e) {
        $.each($(this).parent().find("td.editable"), function(){
            employerBuffer[$(this).data('name')] = $(this).text().trim();
            var steps = $(this).data("numberstep") ? ' step="' + $(this).data("numberstep") + '" ' : '';
            $(this).html('<input type="' + $(this).data("formtype") + '"' + steps + ' class="form-control col-md-12" value="' + $(this).text().trim() + '" />');
            $(this).toggleClass('editable editmode');
        });
        $(this).parent().find('td:last-child').hasClass('editbuttons')?{}:
            $(this).parent().find('td:last-child')
                .prepend('<button class="btn btn-default fa fa-check save-changes"></button><button class="btn btn-default fa fa-times dismiss-changes"></button>')
                .addClass('editbuttons');
    } );
    $('#employers').on( 'click', 'button.delete-column', function () {
        var row = $(this).parent().parent();
        var id = row.data("employerid");
        $.ajax({
            url: '/mitarbeiter/'+id,
            type: 'delete',
        }).done(function (d) {
            if(d==1)
                $(row).fadeOut("slow", function() { $(this).remove(); } );

            //errorhandling
        });
    });
    $('#employers').on( 'click', 'button.save-changes', function () {
        var data = {};
        var row = $(this).parent().parent();
        data.id = row.data("employerid");

        $.each(row.find("td.editmode"), function(){
            data[$(this).data('name')] = $(this).find('input').val();
            $(this).text($(this).find('input').val());
            $(this).toggleClass('editable editmode');
        });
        $.ajax({
            url: '/mitarbeiter/'+data.id,
            type: 'patch',
            data: data
        }).done(function () {
            $(row).find('button.save-changes').parent().removeClass('editbuttons')
            $(row).find('button.save-changes').remove();
            $(row).find('button.dismiss-changes').remove();
        });
    });
    $('#employers').on( 'click', 'button.dismiss-changes', function () {
        var row = $(this).parent().parent();

        $.each(row.find("td.editmode"), function(){
            $(this).text(employerBuffer[$(this).data('name')]);
            $(this).toggleClass('editable editmode');
        });

        $(row).find('button.save-changes').parent().removeClass('editbuttons')
        $(row).find('button.save-changes').remove();
        $(row).find('button.dismiss-changes').remove();
    });


    $('#customers').DataTable();
});