<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Employer;
use App\Order;
use App\Article;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::where('closed','==',false)->get();
        $customers = Customer::all();
        $employers = Employer::all();
        return View('Order.Index',['orders' => $orders, 'customers' => $customers, 'employers' => $employers]);
    }


    public function closeOrder(Order $order)
    {
        $order->closed = true;
        $order->save();

        return redirect('/bestellung/bearbeiten/' . $order->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new order;

        $order->customer_id = $request['customer'];
        $order->employer_id = $request['employer'];
        $order->delivery_date = date("Y-m-d h:m:i",strtotime($request['deliverydate']));

        $order->save();

        return redirect('/bestellung/bearbeiten/' . $order->id);
    }

    public function addArticle(Order $order, Article $article){
        $order->addArticle($article->id);
        return redirect('/bestellung/bearbeiten/' . $order->id);
    }

    public function removeArticle(Order $order, Article $article){
        $order->removeArticle($article->id);
        return redirect('/bestellung/bearbeiten/' . $order->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $articleCollection = Article::all();
        return View('Order.Edit',['order' => $order, 'orderArticles' => $order->articles->groupBy("id"), 'articles' => $articleCollection]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->id = $request['orderid'];

        $order->save();
        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        return Order::destroy($order->id);
    }
}
