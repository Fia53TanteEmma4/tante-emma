<?php

namespace App\Http\Controllers;

use \App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function Index(){

        $articleCount = App\Article::all()->count();
        $articleTotalCount = App\Article::sum('count');
        $orderCount = App\Order::where('closed','==', false)->get()->count();
        $customerCount = App\Customer::all()->count();
        $employerCount = App\Employer::all()->count();
        return View('Home.Index',['articleCount' => $articleCount, 'articleTotalCount' => $articleTotalCount, 'orderCount' => $orderCount, 'customerCount' => $customerCount, 'employerCount' => $employerCount]);
    }

    public function Test(){
    	return DB::table('articles')->get();
    }
}
