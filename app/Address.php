<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function Customers() {
        return $this->belongsTo(Customer::class);
    }
}
