<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employer extends User
{

    public function address()
    {
        return $this->hasOne('App\Address','user_id');
    }
}
