<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
   public function articles(){
   	 return $this->belongsToMany(Article::class);
   }

     public function customer(){
         return $this->belongsTo(Customer::class);
     }

     public function employer(){
         return $this->belongsTo(Employer::class);
     }


    public function price() {
       return $this->articles->sum('price');
    }

    public function addArticle(int $articleId) {
       if($this->id != null && $this->id > 0){
           $article = Article::find($articleId);
           $article->count = $article->count -1;
           $article->save();

           $this->articles()->save($article);
       }

       $this->load('articles');
       return $this->articles;
    }

    public function removeArticle(int $articleId) {
       $articleCount = $this->articles()->detach($articleId);

       $article = Article::find($articleId);
       $article->count = $article->count + $articleCount;
       $article->save();

       $this->load('articles');
       return $this->articles;
    }
}
