@extends('Layouts.Layout')
@section('title')
    Kunden
@endsection
@section('siteScripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection
@section('content')
    <div id="page-wrapper">
        <h2>Übersicht Mitarbeiter</h2>
        <div class="row">
            <div class="col-lg-12">
                <table id="employers" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Adresse</th>
                        <th>Berechtigungen</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($employers as $employer)
                        <tr data-employerid="{{$employer->id}}">
                            <td data-formtype="text" data-name="employername" class="editable">{{$employer->name}}</td>
                            <td><a href="#">Adresse(n) anzeigen</a></td>
                            <td data-formtype="text" data-name="permission" class="editable">{{$employer->permission}}</td>
                            <td class="col-md-2">
                                <button class="btn btn-danger fa fa-trash-o delete-column"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <h3>Mitarbeiter hinzufügen</h3>
        <div class="row">
            <div class="col-md-5">
                <form class="form-horizontal" method="POST" action="/mitarbeiter">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="name" name="employername" placeholder="Martina Mustermann" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="permission">Beriechtigungen</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="permission" name="permission" placeholder="keine">
                        </div>
                    </div>
                    <button type="submit" class="col-md-offset-4 col-md-4 btn btn-default">Speichern</button>
                </form>
            </div>
            <div class="col-md-7">
                <a href="#">+ Adresse hinzufügen</a>
            </div>
        </div>
    </div>
@endsection