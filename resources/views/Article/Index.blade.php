@extends('Layouts.Layout')
@section('title')
    Artikel
@endsection
@section('siteScripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection
@section('content')
    <div id="page-wrapper">
        <h2>Übersicht Artikel</h2>
        <div class="row">
            <div class="col-lg-12">
                <table id="articles" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Bezeichnung</th>
                        <th>Anzahl</th>
                        <th>Preis in €</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($articles as $article)
                        @if($article->count <= 3)
                            @if($article->count <= 0)
                                <tr class="danger" data-articleId="{{$article->id}}">
                            @else
                                <tr class="warning" data-articleId="{{$article->id}}">
                            @endif
                                    <td data-formtype="text" data-n ame="articlename" class="editable">{{$article->name}}</td>

                                    <td data-formtype="number" data-name="count" class="editable">
                                        {{$article->count}}
                                        <span class="pull-right fa fa-warning" data-toggle="tooltip" data-placement="left" title="niedriger Bestand!"></span>
                                    </td>
                        @else
                            <tr data-articleId="{{$article->id}}">
                                <td data-formtype="text" data-name="articlename" class="editable">{{$article->name}}</td>
                                <td  data-formtype="number" data-name="count" class="editable">{{$article->count}}</td>
                        @endif
                            <td  data-formtype="number" data-numberstep="0.01" data-name="price" class="editable">{{$article->price}}</td>
                            <td class="col-md-2">
                                <button class="btn btn-danger fa fa-trash-o delete-column"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <h3>Artikel hinzufügen</h3>
        <div class="row">
            <form class="form-horizontal" method="POST" action="/artikel">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-1 control-label" for="article">Artikelname</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="article" name="article" placeholder="Milch" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label" for="price">Preis in €</label>
                    <div class="col-md-2">
                        <input type="number" step="0.01" class="form-control" id="price" name="price" placeholder="1.99" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label" for="count">Anzahl</label>
                    <div class="col-md-2">
                        <input type="number" class="form-control" id="count" name="count" placeholder="10" required>
                    </div>
                </div>
                <button type="submit" class="col-md-offset-1 col-md-1 btn btn-default">Speichern</button>
            </form>
        </div>
    </div>
@endsection