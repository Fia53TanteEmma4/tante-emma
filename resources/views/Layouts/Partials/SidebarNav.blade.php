<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="/bestellungen"><i class="fa fa-shopping-cart fa-fw"></i> Bestellungen</a>
            </li>
            <li>
                <a href="/artikel"><i class="fa fa-cubes fa-fw"></i> Artikel</a>
            </li>
            <li>
                <a href="/kunden"><i class="fa fa-users fa-fw"></i> Kunden</a>
            </li>
            <li>
                <a href="/mitarbeiter"><i class="fa fa-user fa-fw"></i> Mitarbeiter</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->