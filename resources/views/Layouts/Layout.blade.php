<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{URL::asset('css/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{URL::asset('css/sb-admin-2.min.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{URL::asset('css/morris.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- summernote -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">

    <!-- dataTables -->
    <link href="{{URL::asset('css/dataTables.responsive.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">
    @include('Layouts.Partials.Nav')

    @yield('content')

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{URL::asset('js/metisMenu.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{URL::asset('js/raphael.min.js')}}"></script>
<script src="{{URL::asset('js/morris.min.js')}}"></script>
<script src="{{URL::asset('js/morris-data.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{URL::asset('js/sb-admin-2.js')}}"></script>
<script src="{{URL::asset('js/site.js')}}"></script>
<script src="{{URL::asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/dataTables.bootstrap.js')}}"></script>
<script src="{{URL::asset('js/dataTables.responsive.js')}}"></script>

@yield('siteScripts')

</body>

</html>
