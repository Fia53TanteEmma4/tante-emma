@extends('Layouts.Layout')
@section('title')
    Kunden
@endsection
@section('siteScripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
@section('content')
    <div id="page-wrapper">
        <h2>Übersicht Kunden</h2>
        <div class="row">
            <div class="col-lg-12">
                <table id="customers" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Telefon</th>
                        <th>Adresse</th>
                        <th>Liefernotiz</th>
                        <th>Stammkunde</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($customers as $customer)
                        <tr data-customerId="{{$customer->id}}">
                            <td data-formtype="text" data-name="customername" class="editable">{{$customer->name}}</td>
                            <td data-formtype="text" data-name="telephone" class="editable">{{$customer->telephone}}</td>
                            <td><span>{{$customer->address->street}}, {{$customer->address->postalcode}}, {{$customer->address->city}}</span></td>
                            <td data-formtype="text" data-name="deliverynote" class="editable">{{$customer->delivery_note}}</td>
                            <td>
                                @if($customer->is_regular)
                                    <span class="fa fa-check"></span>
                                @else
                                    <span class="fa fa-times"></span>
                                @endif
                            </td>
                            <td class="col-md-2">
                                <button class="btn btn-danger fa fa-trash-o delete-column"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <h3>Kunde hinzufügen</h3>
        <div class="row">
            <div class="col-md-5">
                <form class="form-horizontal" method="POST" action="/kunden">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="name" name="customername" placeholder="Max Mustermann" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tel">Telefonnummer</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="tel" name="telephone" placeholder="++49/">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="delivery">Liefernotiz</label>
                        <div class="col-md-8">
                            <textarea cols="5" class="form-control" id="delivery" name="deliverynote" placeholder="Notiz"></textarea>
                        </div>
                    </div>
                    <fieldset>
                        <legend>Adresse</legend>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <input type="text" class="form-control" id="street" name="street" placeholder="Straße/Nr.">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-4">
                                <input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="PLZ">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="city" name="city" placeholder="Stadt">
                            </div>
                        </div>
                    </fieldset>
                    <button type="submit" class="col-md-offset-4 col-md-4 btn btn-primary">Speichern</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Adressen anzeigen</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection