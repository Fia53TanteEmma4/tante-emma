@extends('Layouts.Layout')
@section('title')
    Bestellung
@endsection
@section('siteScripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection
@section('content')
    <div id="page-wrapper">
        <h2>Übersicht Bestellungen</h2>
        <div class="row">
            <div class="col-lg-12">
                <table id="" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Kundenname</th>
                        <th>Anzahl Artikel</th>
                        <th>Preis</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($orders as $order)

                        <tr data-orderid="{{$order->id}}">
                            <td>{{$order->customer["name"]}}</td>
                            <td>{{$order->articles->count()}}</td>
                            <td>{{$order->price()}}</td>
                            <td class="col-md-2">
                                <a href="/bestellung/bearbeiten/{{$order->id}}" class="btn btn-default fa fa-edit edit-column"></a>
                                <button class="btn btn-danger fa fa-trash-o delete-column"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <h3>Neue Bestellung</h3>
        <div class="row">
            <form class="form-horizontal" method="POST" action="/bestellungen">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-1 control-label" for="article">Kunde</label>
                    <div class="col-md-2">
                        <select class="form-control" name="customer" required>
                            <option></option>
                            @foreach($customers as $customer)
                                <option value="{{$customer->id}}">{{$customer->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label" for="article">Mitarbeiter</label>
                    <div class="col-md-2">
                        <select class="form-control" name="employer" required>
                            <option></option>
                            @foreach($employers as $employer)
                                <option value="{{$employer->id}}">{{$employer->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label" for="article">Lieferdatum</label>
                    <div class="col-md-2">
                        <input type="date" name="deliverydate" value="{{date("d. M Y", time() + 86400)}}" required>
                    </div>
                </div>
                <button type="submit" class="col-md-offset-1 col-md-1 btn btn-default">erstellen</button>
            </form>
        </div>
    </div>
@endsection