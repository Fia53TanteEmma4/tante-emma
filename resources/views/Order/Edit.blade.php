@extends('Layouts.Layout')
@section('title')
    Bestellung
@endsection
@section('siteScripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection
@section('content')
    <div id="page-wrapper">
            <h2>Bestellnumer: {{$order->id}}</h2>
            <div class="row">
            <div class="col-md-offset-8 col-md-4">
                @if($order->closed)
                    <h3>Bestellung ist abgeschlossen</h3>
                @else
                    <form method="post" action="/bestellung/{{$order->id}}/abschluss">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PATCH">
                        <button type="submit" class="btn btn-success">Bestellung abschließen</button>
                    </form>
                @endif
            </div>
        </div>
        <div>vom {{date("d. M y", strtotime($order->created_at))}}</div>
        <div>Lieferdatum: {{date("d. M y", strtotime($order->delivery_date))}}</div>
        <hr>
        <div class="row">
            <span class="col-md-6">Kunde: {{$order->customer->name}}</span>
            <span class="col-md-6">Mitarbeiter: {{$order->employer->name}}</span>
        </div>
        <div class="row">
            <span class="col-md-12"> Liefernotiz: {{$order->customer->delivery_note}}</span>
        </div>
        <hr>
        <div class="row">
            <span class="col-md-1"> Adresse:</span>
            <div class="col-md-11">
                <div class="row">
                    <span class="col-md-1">Straße/Nr:</span>
                    <span class="col-md-8">{{$order->customer->address->street}}</span>
                </div>
                <div class="row">
                    <span class="col-md-1">PLZ:</span>
                    <span class="col-md-8">{{$order->customer->address->postalcode}}</span>
                </div>
                <div class="row">
                    <span class="col-md-1">Stadt:</span>
                    <span class="col-md-8">{{$order->customer->address->city}}</span>
                </div>
            </div>
        </div>

        <h3>Bestellung ({{$order->price()}} €)</h3>
        <div class="row">
            <div class="col-lg-12">
                <table id="" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Artikel</th>
                        <th>Anzahl</th>
                        <th>Preis</th>
                        <th>Preis gesammt</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($orderArticles as $article)
                        <tr>
                            <td>{{$article->first()->name}}</td>
                            <td>{{sizeof($article)}}</td>
                            <td>{{$article->first()->price}}</td>
                            <td>{{$article->first()->price * sizeof($article)}}</td>
                            <td class="col-md-2">
                                @if(!$order->closed)
                                    <form method="post" action="/bestellung/{{$order->id}}/artikel/{{$article->first()->id}}">
                                        {{csrf_field()}}
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button type="submit" class="btn btn-danger fa fa-trash-o delete-article"></button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <h3>Artikel zu aktueller Bestellung hinzufügen</h3>
        <div class="row">
            <div class="col-lg-12">
                <table id="articles" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Bezeichnung</th>
                        <th>Lagerbestand</th>
                        <th>Preis in €</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($articles as $article)
                        @if($article->count <= 3)
                            @if($article->count <= 0)
                                <tr class="danger">
                            @else
                                <tr class="warning">
                                    @endif
                                    <td>{{$article->name}}</td>

                                    <td>
                                        {{$article->count}}
                                        <span class="pull-right fa fa-warning" data-toggle="tooltip" data-placement="left" title="niedriger Bestand!"></span>
                                    </td>
                                @else
                                    <tr data-articleId="{{$article->id}}">
                                        <td>{{$article->name}}</td>
                                        <td>{{$article->count}}</td>
                                        @endif
                                        <td>{{$article->price}}</td>
                                        <td class="col-md-2">
                                            @if(!$order->closed)
                                                <form method="post" action="/bestellung/{{$order->id}}/artikel/{{$article->id}}">
                                                    {{csrf_field()}}
                                                    <button type="submit" class="btn btn-default fa fa-plus add-article"></button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection